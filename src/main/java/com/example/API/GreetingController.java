package com.example.API;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/about")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
       return new Greeting(counter.incrementAndGet(), String.format(template, name));

    }
    @GetMapping("/test")
    public Greeting test(@RequestParam(value = "name", defaultValue = "testing in place") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }
}